package id.ub.papb.rizal;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import id.ub.papb.rizal.R;


public class MainActivity2 extends AppCompatActivity {
    TextView operasihitung,hasiloperasi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        operasihitung = findViewById(R.id.operasihitung);
        hasiloperasi = findViewById(R.id.hasiloperasi);

        Intent intent = getIntent();
        String hasil = intent.getStringExtra("Hitung");
        String operasi = intent.getStringExtra("Operasi");

        operasihitung.setText(hasil);
        hasiloperasi.setText(operasi);
    }
}
