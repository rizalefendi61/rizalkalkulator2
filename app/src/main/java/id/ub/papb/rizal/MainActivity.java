package id.ub.papb.rizal;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import id.ub.papb.rizal.R;

public class MainActivity extends AppCompatActivity {
    Button button1,button2,button3,button4,button5,button6,button7,button8,button9,button0;
    Button buttontambah,buttonkurang,buttonkali,buttonbagi,buttonkoma,buttonhasil,buttonclear;
    TextView operasi;
    float bil1, bil2;
    boolean tambah, kurang, kali, bagi;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context = this;
        button1 = findViewById(R.id.button1);
        button2 = findViewById(R.id.button2);
        button3 = findViewById(R.id.button3);
        button4 = findViewById(R.id.button4);
        button5 = findViewById(R.id.button5);
        button6 = findViewById(R.id.button6);
        button7 = findViewById(R.id.button7);
        button8 = findViewById(R.id.button8);
        button9 = findViewById(R.id.button9);
        button0 = findViewById(R.id.button0);
        buttontambah = findViewById(R.id.buttontambah);
        buttonkurang = findViewById(R.id.buttonkurang);
        buttonkali = findViewById(R.id.buttonkali);
        buttonbagi = findViewById(R.id.buttonbagi);
        buttonkoma = findViewById(R.id.buttonkoma);
        buttonhasil = findViewById(R.id.buttonhasil);
        buttonclear = findViewById(R.id.buttonclear);
        operasi = findViewById(R.id.operasi);

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                operasi.setText(operasi.getText() + "1");
            }
        });
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                operasi.setText(operasi.getText() + "2");
            }
        });
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                operasi.setText(operasi.getText() + "3");
            }
        });
        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                operasi.setText(operasi.getText() + "4");
            }
        });
        button5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                operasi.setText(operasi.getText() + "5");
            }
        });
        button6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                operasi.setText(operasi.getText() +"6");
            }
        });
        button7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                operasi.setText(operasi.getText() +"7");
            }
        });
        button8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                operasi.setText(operasi.getText() +"8");
            }
        });
        button9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                operasi.setText(operasi.getText() + "9");
            }
        });
        button0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                operasi.setText(operasi.getText() + "0");
            }
        });
        buttontambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (operasi == null) {
                    operasi.setText("");
                } else {
                    bil1 = Float.parseFloat(operasi.getText() + "");
                    tambah = true;
                    operasi.setText(null);
                }
            }
        });
        buttonkurang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bil1 = Float.parseFloat(operasi.getText() + "");
                kurang= true;
                operasi.setText(null);
            }
        });
        buttonkali.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bil1 = Float.parseFloat(operasi.getText() + "");
                kali = true;
                operasi.setText(null);
            }
        });
        buttonbagi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bil1 = Float.parseFloat(operasi.getText() + "");
                bagi = true;
                operasi.setText(null);
            }
        });
        buttonkoma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                operasi.setText(operasi.getText() + ".");
            }
        });
        buttonclear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                operasi.setText("");
            }
        });
        buttonhasil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bil2 = Float.parseFloat(operasi.getText() + "");
                String hitung = "";
                if (tambah == true) {
                    operasi.setText(bil1 + bil2 + "");
                    hitung = bil1 + " + " + bil2;
                    tambah = false;
                }
                if (kurang == true) {
                    operasi.setText(bil1 - bil2 + "");
                    hitung = bil1 + " - " + bil2;
                    kurang = false;
                }
                if (kali == true) {
                    operasi.setText(bil1 * bil2 + "");
                    hitung = bil1 + " * " + bil2;
                    kali = false;
                }
                if (bagi == true) {
                    operasi.setText(bil1 / bil2 + "");
                    hitung = bil1 + " / " + bil2;
                    bagi = false;
                }
                Intent intent =new Intent (context,MainActivity2.class);
                intent.putExtra("Operasi",operasi.getText().toString());
                intent.putExtra("Hitung", hitung);
                startActivity(intent);
            }
        });
    }
}
